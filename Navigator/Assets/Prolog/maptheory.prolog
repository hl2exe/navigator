%%% ---------------------------------------------------------
%%% UTILITY
%%% ---------------------------------------------------------

:- public getGameController/1, getFileMapDownloader/1.

getGameController(GameController) :-
	Controller is $typeutils.findtype("GameController"),
	GameController is $object.findobjectoftype(Controller).

getFileMapDownloader(MapDownloader) :-
	Downloader is $typeutils.findtype("FileMapDownloader"),
	MapDownloader is $object.findobjectoftype(Downloader).

%%% ---------------------------------------------------------
%%% MAP MANAGEMENT
%%% ---------------------------------------------------------

loadMap(Filename) :-
	checkParameters([Filename], [VFilename]),
	getFileMapDownloader(MapDownloader),
	MapDownloader.loadMap(VFilename).

map(DimX, DimY) :-
	getGameController(GameController),
	GameController.showMap(DimX, DimY).

mapdata(Index, element(X,Z)) :-
	getGameController(GameController),
	GameController.createWall(X,Z).

subscribe(Topic) :-
	checkParameters([Topic], [VTopic]),	
	getMqttClient(MqttClientComponent),
	MqttClientComponent.topics.add(VTopic),
	MqttClientComponent.subscribe().
