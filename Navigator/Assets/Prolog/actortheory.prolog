%%% ---------------------------------------------------------
%%% ACTOR MANAGEMENT
%%% ---------------------------------------------------------

createActor(Name, StartX, StartZ) :-
	verifyName(Name) -> !, fail.

createActor(Name, StartX, StartZ) :-
	not(verifyName(Name)),
	getGameController(GameController),
	GameController.createactor(Name, StartX, StartZ).

createActor(Name, Prefab, StartX, StartZ) :-
	verifyName(Name) -> !, fail.

createActor(Name, Prefab, StartX, StartZ) :-
	checkParameters([Name, Prefab], [VName, VPrefab]),
	not(verifyName(VName)),
	getGameController(GameController),
	GameController.instance.createactor(Name, VPrefab, StartX, StartZ).

verifyName(Name) :-
	checkParameters([Name], [VName]),
	Found is $gameobject.find(VName),
	Found == null,
	!,
	fail.

verifyName(Name) :-
	checkParameters([Name], [VName]),
	Found is $gameobject.find(VName),
	Found \== null.

%%% ---------------------------------------------------------
%%% MOVEMENT
%%% ---------------------------------------------------------

move(Name, Direction, X, Z) :-
	not(verifyName(Name)) -> !,fail.

move(Name, Direction, X, Z) :-
	verifyName(Name),
	checkParameters([Name, Direction], [VName, VDirection]),
	!,
	ActorData is $'ActorUtility'.buildActorDataHandlerData(VName, X, Z, VDirection),
	getUtility(Utility),
	Utility.notifyData(ActorData).

%%% ---------------------------------------------------------
%%% OBSTACLE
%%% ---------------------------------------------------------

senseObstacle(Name) :-
	not(verifyName(Name)) -> !, fail.

senseObstacle(Name) :-
	verifyName(Name),
	checkParameters([Name], [VName]),
	SensorData is $'ActorUtility'.buildSenseDataHandlerData(VName),
	getUtility(Utility),
	Utility.notifyData(SensorData).

% Currently used since guimanager actor is the only one connected to unity
senseObstacle :-
	SensorData is $'ActorUtility'.buildSenseDataHandlerData("guimanager"),
	getUtility(Utility),
	Utility.notifyData(SensorData).



	