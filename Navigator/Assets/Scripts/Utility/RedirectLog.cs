﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class RedirectLog : MonoBehaviour
{
    static readonly Dictionary<LogType, string> logTypeColors = new Dictionary<LogType, string>()
    {
        { LogType.Assert, "green" },
        { LogType.Error, "red" },
        { LogType.Exception, "red" },
        { LogType.Log, "black" },
        { LogType.Warning, "yellow" },
    };

    private Text _text;

    void Start()
    {
        _text = GetComponent<Text>();
        _text.supportRichText = true;
    }

    private void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        // Remove callback when object goes out of scope
        Application.logMessageReceived -= HandleLog;
    }

    private void HandleLog(string logString, string stackTrace, LogType type)
    {
        string color;
        logTypeColors.TryGetValue(type, out color);
        logString = string.Format("<color=" + color + ">{0}</color>" + Environment.NewLine, logString);
        _text.text += logString;
    }
}
