﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ShowHideElement : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            var canvasGroup = GetComponent<CanvasGroup>();

            if (canvasGroup.alpha == 1)
                canvasGroup.alpha = 0;
            else
                canvasGroup.alpha = 1;
        }
    }
}