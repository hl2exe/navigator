﻿using UnityActorSimulator;
using UnityEngine;
using UniRx;
using System.Text;
using System;
using Prolog;
using System.Linq;

/// <summary>
/// Allows to download a prolog grid map via MQTT.
/// Whenever a complete map is downloaded, an event called subscribeACK is emitted.
/// </summary>
public class MapDownloader : MonoBehaviour
{
    private void Start()
    {
        var client = MqttClientComponent.Instance;
        var kb = GameObject.Find("GlobalKB").GetComponent<KB>();

        client.PublishedObs
            .ObserveOnMainThread()
            .Subscribe(args =>
            {
                Debug.Log("Someone published a message!");
                var message = Encoding.UTF8.GetString(args.Message);

                foreach (var line in message.Split(new string[] { Environment.NewLine, "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    Debug.Log("Retrieved: " + line);

                    if (line.StartsWith("map"))
                        kb.IsTrueParsed(line + ".");
                    
                }
                
                var handler = TcpConnectorListener.Instance.Handlers.Values.ToList<TcpActorClientHandler>()[0];

                if (handler != null)
                {
                    var downloadACK = UnityPrologUtility.BuildEvent("subscribeACK", name, "subscribeACK");
                    handler.Send<string>(downloadACK);
                    Debug.Log("[MapDownloader] Sending subscribeACK!");
                }
                else
                    Debug.LogError("[MapDownloader] No client found");
            })
            .AddTo(this);

    }
}