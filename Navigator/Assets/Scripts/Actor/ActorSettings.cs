﻿using System;
using System.Collections.Generic;
using UnityActorSimulator;

/// <summary>
/// Actor's grid movement settings wrapper class
/// </summary>
[Serializable]
public class ActorSettings : BaseActorSettings
{
    public List<string> directions;
    public float movementSpeed;
    public float rotationSpeed;

    public ActorSettings(string actorName, List<string> directions, float movementSpeed, float rotationSpeed) : base(actorName)
    {
        this.directions = directions;
        this.movementSpeed = movementSpeed;
        this.rotationSpeed = rotationSpeed;
    }

    public override string ToString()
    {
        return "ActorSettings for " + base.ToString()
            + "Key bindings: " + directions.ToString() + Environment.NewLine
            + "Speed factor: " + movementSpeed + Environment.NewLine
            + "Rotation speed: " + rotationSpeed + Environment.NewLine;
    }
}

