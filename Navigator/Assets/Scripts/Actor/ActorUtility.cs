﻿/// <summary>
/// Simple utility class that implements some basic actor structures
/// </summary>
public static class ActorUtility
{
    public static ActorDataHandlerArgs BuildActorDataHandlerData(string name, int positionX, int positionZ, string direction)
    {
        return new ActorDataHandlerArgs(name, positionX, positionZ, direction);
    }

    public static SenseDataHandlerArgs BuildSenseDataHandlerData(string name)
    {
        return new SenseDataHandlerArgs(name);
    }
}
