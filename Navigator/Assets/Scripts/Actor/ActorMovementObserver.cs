﻿using System;
using System.Collections;
using System.Linq;
using UniRx;
using UnityActorSimulator;
using UnityEngine;

/// <summary>
/// This class connects the sintatic dimension of the application's business logic
/// with the Unity's implementation of it. In other words, by exploiting a Mapper class,
/// the ActorMovementObserver is able to define for each command a related implementation of it
/// within the Unity simulation context.
/// More precisely, in this 8-connected grid simulation, each command refers to a tile position to reach.
/// Therefore a general command is expressed as Direction and X,Y coordinates of the tile.
/// The associated implementation is composed by a rotation and a translation, necessary in order to
/// correctly reach the tile. Moreover, each incoming command is stored in a queue if another one is currently
/// being executed.
/// </summary>
public class ActorMovementObserver : MonoBehaviour
{
    ActorSettingsMapper _configuration;
    private IDisposable _currentAction;
    private ActorAnimator _anim;

    private Queue _commands;

    // Use this for initialization
    void Start ()
    {
        // Queue
        _commands = new Queue();

        // Animator [optional]
        _anim = GetComponent<ActorAnimator>();

        // Retrieving mapper
        var settings = SettingsDatabase.Instance.Settings;
        object retrievedValue = null;
        settings.TryGetValue(gameObject.name, out retrievedValue);
        _configuration = retrievedValue as ActorSettingsMapper;

        // Subscribing in order to receive commands wrapped into ActorDataHandlerArgs instances
        UnityPrologUtility.inputObs.Where(data => data is ActorDataHandlerArgs && ((ActorDataHandlerArgs)data).Name.Equals(name))
            .Subscribe(data =>
            {
                var retrieved = data as ActorDataHandlerArgs;

                if (_currentAction != null)
                {
                    Debug.Log("Received new command! Adding to queue..");
                    _commands.Enqueue(retrieved);
                }
                else
                {
                    Debug.Log("Received movement data: " + retrieved);
                    ApplyMovement(retrieved);
                }

            })
            .AddTo(this);

    }

    /// <summary>
    /// A movement action is composed by a rotation (RotationCoroutine)
    /// and a translation (MovementCoroutine)
    /// </summary>
    /// <param name="retrieved"></param>
    private void ApplyMovement(ActorDataHandlerArgs retrieved)
    {
        if (_anim != null)
            _anim.Walk();

        _currentAction = Observable.FromMicroCoroutine(() => RotationCoroutine(retrieved.Direction))
                    .Subscribe(
                    _ => { },
                    _ => { },
                    () =>
                    {
                        _currentAction = Observable.FromMicroCoroutine(() => MovementCoroutine(retrieved.PositionX, retrieved.PositionZ))
                            .Subscribe(
                                _ => { },
                                _ => { },
                                () =>
                                {
                                    var eventString = UnityPrologUtility.BuildEvent("moveACK", name, "moveACK");
                                    var handler = TcpConnectorListener.Instance.GetHandlerFromID(name);
                                    handler.Send<string>(eventString);
                                    Debug.Log("[ActorMovementObserver] Movement action ended, synchronizing with controller! Sending event...");

                                    if (_anim != null)
                                        _anim.Idle();

                                    _currentAction = null;

                                    // Extracting queued command
                                    if (_commands.Count > 0)
                                    {
                                        var enqueued = _commands.Dequeue();

                                        Debug.Log("Extracting command from queue");
                                        ApplyMovement(enqueued as ActorDataHandlerArgs);
                                    }
                                });
                    })
                    .AddTo(this);
    }

    IEnumerator RotationCoroutine(string direction)
    {
        MapperInfo info = _configuration.GetMappingInfoFromDirection(direction);
        Vector3 eulerAngles = transform.rotation.eulerAngles;
        float rotationSpeed = _configuration.RotationSpeed;
        float rotationAccumulator = 0;

        float deltaAngle = Math.Abs(info.angle - eulerAngles.y);
        int sign = Math.Sign(info.angle - eulerAngles.y);

        if (deltaAngle > 180)
            deltaAngle = 180 - deltaAngle;

        // Rotation
        while(EvaluateRotationDelta(rotationAccumulator, deltaAngle) > 0)
        {
            float deltaY = EvaluateRotationDelta(rotationAccumulator, deltaAngle) * Time.fixedDeltaTime * rotationSpeed;
            eulerAngles.y += deltaY * sign;
            rotationAccumulator += deltaY;

            if (eulerAngles.y > 360)
                eulerAngles.y -= 360;
            else if (eulerAngles.y < -360)
                eulerAngles.y += 360;


            transform.eulerAngles = eulerAngles;

            yield return null; // next frame
        }

        transform.eulerAngles = new Vector3(transform.eulerAngles.x, info.angle, transform.eulerAngles.z);
    }

    IEnumerator MovementCoroutine(int positionX, int positionZ)
    {
        float movementSpeed = _configuration.MovementSpeed;
        var tileGenerator = GameController.Instance.TileGenerator;
        var tileInstance = tileGenerator.Tiles[positionX * ((int) tileGenerator.GridDimensions.y) + positionZ];

        Debug.Log(tileInstance);

        // Movement
        while (!EvaluateMovementCondition(tileInstance.transform.position))
        {
            transform.position += transform.forward * movementSpeed * Time.fixedDeltaTime;

            yield return null; // next frame
        }

        transform.position = new Vector3(tileInstance.transform.position.x, transform.position.y, tileInstance.transform.position.z);
    }

    private float EvaluateRotationDelta(float rotationAccumulator, float deltaAngle)
    {
        if (rotationAccumulator >= deltaAngle)
            return 0;
        else
            return deltaAngle;
    }

    private bool EvaluateMovementCondition(Vector3 tilePosition)
    {
        Vector3 modifiedTilePosition = new Vector3(tilePosition.x, transform.position.y, tilePosition.z);

        return Vector3.Distance(transform.position, modifiedTilePosition) <= 0.1f;
    }
}
