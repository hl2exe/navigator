﻿using System;

/// <summary>
/// Move action wrapper class
/// </summary>
public class ActorDataHandlerArgs : EventArgs
{
    public string Name { get; set; }
    public string Direction { get; set; }
    public int PositionX { get; set; }
    public int PositionZ { get; set; } 

    public ActorDataHandlerArgs(string name, int positionX, int positionZ, string direction)
    {
        Name = name;
        PositionX = positionX;
        PositionZ = positionZ;
        Direction = direction;
    }

    public override string ToString()
    {
        return Name + ", " + Direction + ", position(" + PositionX + "," + PositionZ + ")";
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        if (obj is ActorDataHandlerArgs == false)
            return false;

        var retrieved = obj as ActorDataHandlerArgs;

        return Name.Equals(retrieved.Name) &&
            Direction.Equals(retrieved.Direction) &&
            PositionX == retrieved.PositionX &&
            PositionZ == retrieved.PositionZ;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

}