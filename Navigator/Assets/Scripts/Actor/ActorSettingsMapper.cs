﻿using System.Collections.Generic;

public sealed class ActorSettingsMapper
{
    private IDictionary<string, MapperInfo> _directionMappings = new Dictionary<string, MapperInfo>()
    {
        { "n", MapperInfo.north },
        { "e", MapperInfo.east },
        { "s", MapperInfo.south },
        { "w", MapperInfo.west },
        { "ne", MapperInfo.northeast },
        { "nw", MapperInfo.northwest },
        { "se", MapperInfo.southeast },
        { "sw", MapperInfo.southwest }
    };

    private ActorSettings _settings;

    public ActorSettingsMapper(ActorSettings settings)
    {
        _settings = settings; 
    }

    public ActorSettingsMapper(ActorSettings settings, IDictionary<string, MapperInfo> directionMappings)
    {
        _settings = settings;
        _directionMappings = directionMappings;
    }

    public IDictionary<string, MapperInfo> DirectionMappings
    {
        get { return _directionMappings; }
        set { _directionMappings = value; }
    }

    public MapperInfo GetMappingInfoFromDirection(string direction)
    {
        MapperInfo value;
        _directionMappings.TryGetValue(direction, out value);
        return value;
    }

    public float MovementSpeed
    {
        get { return _settings.movementSpeed; }
    }

    public float RotationSpeed
    {
        get { return _settings.rotationSpeed; }
    }
}

public struct MapperInfo
{
    public float angle;

    public MapperInfo(float angle)
    {
        this.angle = angle;
    }

    public static MapperInfo north
    {
        get { return new MapperInfo(0); }
    }

    public static MapperInfo south
    {
        get { return new MapperInfo(180); }
    }

    public static MapperInfo east
    {
        get { return new MapperInfo(90); }
    }

    public static MapperInfo west
    {
        get { return new MapperInfo(270); }
    }

    public static MapperInfo northeast
    {
        get { return new MapperInfo(45); }
    }

    public static MapperInfo northwest
    {
        get { return new MapperInfo(315); }
    }

    public static MapperInfo southeast
    {
        get { return new MapperInfo(135); }
    }

    public static MapperInfo southwest
    {
        get { return new MapperInfo(225); }
    }
}
