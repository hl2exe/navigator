﻿using System;

public class SenseDataHandlerArgs : EventArgs
{
    public string Name { get; set; }

    public SenseDataHandlerArgs(string name)
    {
        Name = name;
    }

    public override string ToString()
    {
        return Name;
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        if (obj is SenseDataHandlerArgs == false)
            return false;

        var retrieved = obj as SenseDataHandlerArgs;

        return Name.Equals(retrieved.Name);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
