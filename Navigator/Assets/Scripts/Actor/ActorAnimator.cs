﻿using UnityEngine;

public class ActorAnimator : MonoBehaviour
{
    public Animator _anim;

    private float _movement;

    public void Start()
    {
        _movement = 0;
        _anim = GetComponent<Animator>();
    }

    public void Walk()
    {
        _movement = 1;
    }

    public void Idle()
    {
        _movement = 0;
    }

    public void FixedUpdate()
    {
        _anim.SetFloat("speed", _movement);
    }
}
