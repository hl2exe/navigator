﻿using System.Linq;
using UniRx;
using UnityActorSimulator;
using UnityEngine;

public class SensorObserver : MonoBehaviour
{
    private void Start()
    {
        UnityPrologUtility.inputObs.Where(data => data is SenseDataHandlerArgs && ((SenseDataHandlerArgs)data).Name.Equals(name))
            .Subscribe(data =>
            {
                // Notifying controller
                if (CheckObstacle(data as SenseDataHandlerArgs))
                {
                    var eventString = UnityPrologUtility.BuildEvent("obstaclefront", name, "obstaclefront");
                    var handler = TcpConnectorListener.Instance.GetHandlerFromID(name);
                    handler.Send<string>(eventString);
                    Debug.Log("[ActorMovementObserver] Detected obstacle above next tile in the plan! Sending event...");
                }

            });
    }


    private bool CheckObstacle(SenseDataHandlerArgs data)
    {
        var gameController = GameController.Instance;

        // Retrieving tile at given indexes
        var gridDimensionZ = gameController.dimensionZ;
        var tileObjPos = gameController.RetrieveTileFromPlayer().transform.position;
        var targetTile = gameController.TileGenerator.Tiles[(int) Mathf.Abs(tileObjPos.x) * gridDimensionZ + (int)Mathf.Abs(tileObjPos.z)];

        // Check if there exists a wall above retrieved tile
        return gameController.WallDelimiter.Walls.ContainsKey(targetTile);

    }
}