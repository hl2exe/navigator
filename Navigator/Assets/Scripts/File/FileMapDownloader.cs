﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityActorSimulator;
using UnityEngine;
using Prolog;

/// <summary>
/// Allows to load a prolog grid map from a file
/// </summary>
public class FileMapDownloader : Singleton<FileMapDownloader>
{
    private KB _kb;

    public string filename;

    private void Start()
    {
        _kb = GameObject.Find("GlobalKB").GetComponent<KB>();
          
        if (string.IsNullOrEmpty(filename) == false)
            LoadMap(filename);
    }

    public void LoadMap(string filename)
    {
        var map = File.ReadAllText(filename);

        foreach (var line in map.Split(new string[] { Environment.NewLine, "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries))
        {
            _kb.IsTrueParsed(line + ".");
        }
    }
}
