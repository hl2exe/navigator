﻿using UnityEngine;
using UnityActorSimulator;

/// <summary>
/// Main class that exposes some utility methods regarding
/// map and actors creation
/// </summary>
public class GameController : Singleton<GameController>
{
    [Range(100,300)]
    public int dimensionX = 100;
    [Range(100, 300)]
    public int dimensionZ = 100;

    public GameObject tilePrefab;
    public GameObject wallPrefab;
    public GameObject actorPrefab;

    private ITileGenerator _tileGenerator;
    private WallDelimiter _wallDelimiter;
    private Vector3 _playerSize;
    private GameObject _actorInstance;

    public void Start()
    {
        Initialize();

        _wallDelimiter = new WallDelimiter(wallPrefab);
    }

    private void Initialize()
    {
        // Player dimensions
        GameObject dummy = GameObject.Instantiate(actorPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        Collider dummyCollider = dummy.GetComponent<Collider>();
        _playerSize = dummyCollider.bounds.size;
        Destroy(dummy);
    }

    public GameObject ActorInstance
    {
        get { return _actorInstance; }
    }

    public Vector3 PlayerSize
    {
        get { return _playerSize; }
    }

    public ITileGenerator TileGenerator
    {
        get { return _tileGenerator; }
        private set { _tileGenerator = value; }
    }

    public WallDelimiter WallDelimiter
    {
        get { return _wallDelimiter; }
        private set { _wallDelimiter = value; }
    }

    public void CreateWall(int x, int z)
    {
        _wallDelimiter.CreateWall(_tileGenerator.Tiles[x * dimensionZ + z]);
    }

    public GameObject RetrieveTileFromPlayer()
    {
        if (_actorInstance != null)
        {
            var actorInstancePosition = _actorInstance.transform.position;

            return TileGenerator.Tiles[(int)Mathf.Abs(actorInstancePosition.x) * dimensionZ + (int)Mathf.Abs(actorInstancePosition.z)];
        }
        else
            return null;

    }

    public void ShowMap(int dimX, int dimZ)
    {
        dimensionX = dimX;
        dimensionZ = dimZ;
        _tileGenerator = new TileGenerator(dimX, dimZ, tilePrefab);
        _tileGenerator.Generate();
    }

    public void CreateActor(string name, int posX, int posZ)
    {
        CreateActor(name, actorPrefab, posX, posZ);
    }

    public void CreateActor(string name, string prefab, int posX, int posZ)
    {
        var prefabObj = Resources.Load<GameObject>(prefab);
        CreateActor(name, prefabObj, posX, posZ);
    }

    public void CreateActor(string name, GameObject prefab, int posX, int posZ)
    {
        GameObject tile = _tileGenerator.Tiles[posX * dimensionZ + posZ];

        Vector3 tilePos = tile.transform.position;
        float tileTop = tilePos.y + TileGenerator.TileSize.y / 2;

        _actorInstance = GameObject.Instantiate
            (
            prefab,
            new Vector3(tilePos.x, tileTop + _playerSize.y / 2, tilePos.z),
            Quaternion.identity
            ) as GameObject;

        _actorInstance.name = name;

        CameraController.Instance.CreateCameraForActor(_actorInstance);
    }
}
