﻿using System.Collections.Generic;
using UnityEngine;
using UnityActorSimulator;
using System.Linq;

/// <summary>
/// The CamweraController script allows to switch between cameras
/// by pressing a specificing key (C).
/// More precisely, whenever an actor is created, it is possible to
/// instantiate a dedicated camera that aims to realize a first person view
/// </summary>
public class CameraController : Singleton<CameraController>
{
    private Dictionary<string, GameObject> _cameras;
    private GameObject _currentActive;

	// Use this for initialization
	void Start ()
    {
        _cameras = new Dictionary<string, GameObject>();
        _cameras.Add("MainCamera", Camera.main.gameObject);
        _currentActive = Camera.main.gameObject;
		
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log("Switching cameras");

            var currentActiveCamera = _currentActive.GetComponent<Camera>();
            currentActiveCamera.enabled = !currentActiveCamera.enabled;

            var cameraList = _cameras.Values.ToList<GameObject>();
            int index = cameraList.IndexOf(_currentActive);

            if (index + 1 >= cameraList.Count)
                index = 0;
            else
                index = index + 1;

            _currentActive = cameraList.ElementAt<GameObject>(index);
            currentActiveCamera = _currentActive.GetComponent<Camera>();
            currentActiveCamera.enabled = !currentActiveCamera.enabled;
        }
    }

    public void CreateCameraForActor(GameObject actor)
    {
        Debug.Log("Creating camera for actor: " + actor.name);
        GameObject cameraObj = new GameObject();

        cameraObj.name = actor.name + " camera";
        cameraObj.AddComponent<Camera>();
        cameraObj.GetComponent<Camera>().enabled = false;

        cameraObj.transform.position = actor.transform.position + new Vector3(0, GameController.Instance.PlayerSize.y, 0);
        cameraObj.transform.parent = actor.transform;
        _cameras.Add(actor.name, cameraObj);
    }
}
