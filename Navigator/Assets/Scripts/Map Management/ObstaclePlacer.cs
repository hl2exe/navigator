﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ObstaclePlacer : MonoBehaviour
{
    private Color _defaultColor;
    private Renderer _renderer;
    private WallDelimiter _wallDelimiter;

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
        _defaultColor = _renderer.material.color;
        _wallDelimiter = GameController.Instance.WallDelimiter;
    }

    private void OnMouseEnter()
    {
        GetComponent<Renderer>().material.color = Color.green;
    }

    private void OnMouseExit()
    {
        GetComponent<Renderer>().material.color = _defaultColor;
    }

    private void OnMouseDown()
    {
        var actorTile = GameController.Instance.RetrieveTileFromPlayer();

        if (_wallDelimiter.Walls.ContainsKey(gameObject) == false && ((actorTile != null && actorTile != gameObject) || actorTile == null))
        {
            _wallDelimiter.CreateWall(gameObject);
        }
    }
}
