﻿using System.Collections.Generic;
using UnityEngine;

public class WallDelimiter
{
    private GameObject _wallPrefab;
    private Dictionary<GameObject, GameObject> _walls;

    private Vector3 _wallSize;

    public WallDelimiter(string wallPrefab)
    {
        _wallPrefab = Resources.Load<GameObject>(wallPrefab);
        _walls = new Dictionary<GameObject, GameObject>();
        Initialize();
    }

    public WallDelimiter(GameObject wallPrefab)
    {
        _wallPrefab = wallPrefab;
        _walls = new Dictionary<GameObject, GameObject>();
        Initialize();
    }

    private void Initialize()
    {
        // Wall dimensions
        GameObject dummy = GameObject.Instantiate(_wallPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        BoxCollider dummyCollider = dummy.GetComponent<BoxCollider>();
        _wallSize = dummyCollider.bounds.size;
        GameObject.Destroy(dummy);
    }

    public Vector3 WallSize
    {
        get { return _wallSize; }
        private set { _wallSize = value; }
    }

    public Dictionary<GameObject, GameObject> Walls
    {
        get { return _walls; }
    }

    public void CreateWall(GameObject tile)
    {

        if (!_walls.ContainsKey(tile))
        {
            float tileTop = tile.transform.position.y + GameController.Instance.TileGenerator.TileSize.y / 2;
            Vector3 tilePos = tile.transform.position;
            Vector3 position = new Vector3(tilePos.x, tileTop + _wallSize.y / 2, tilePos.z);
            GameObject instance = GameObject.Instantiate(_wallPrefab, position, Quaternion.identity) as GameObject;
            _walls.Add(tile, instance);
        }
    }
}