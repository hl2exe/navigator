﻿using System.Collections.Generic;
using UnityEngine;

public interface ITileGenerator
{
    void Generate();
    Vector3 TileSize { get; }
    Vector2 GridDimensions { get; }
    IList<GameObject> Tiles { get; }
}
