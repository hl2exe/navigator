﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TileGenerator : ITileGenerator
{
    private int _dimX;
    private int _dimZ;

    private GameObject _tilePrefab;

    private IList<GameObject> _tiles;

    private Vector3 _tileSize;

    public TileGenerator(int dimX, int dimZ, string tilePrefab)
    {
        _dimX = dimX;
        _dimZ = dimZ;
        _tilePrefab = Resources.Load<GameObject>(tilePrefab);
        _tiles = new List<GameObject>();

        Initialize();
    }

    public TileGenerator(int dimX, int dimZ, GameObject tilePrefab)
    {
        _dimX = dimX;
        _dimZ = dimZ;
        _tilePrefab = tilePrefab;
        _tiles = new List<GameObject>();

        Initialize();
    }

    private void Initialize()
    {
        // Tile dimensions
        GameObject dummy = GameObject.Instantiate(this._tilePrefab, Vector3.zero, Quaternion.identity) as GameObject;
        BoxCollider dummyCollider = dummy.GetComponent<BoxCollider>();
        _tileSize = dummyCollider.bounds.size;
        GameObject.Destroy(dummy);
    }

    public Vector2 GridDimensions
    {
        get { return new Vector2(_dimX, _dimZ); }
    }

    public Vector3 TileSize
    {
        get { return _tileSize; }
        private set { _tileSize = value; }
    }

    public IList<GameObject> Tiles
    {
        get { return this._tiles; }
    }

    public void Generate()
    {
        Debug.Log("[TileGenerator] Grid Dimensions: " + _dimX * _dimZ);

        for (int i = 0; i < _dimX * _dimZ; i++)
        {
            int dx = i / _dimX;
            int dz = i % _dimZ;
            GameObject tileInstance = GameObject.Instantiate(_tilePrefab, new Vector3(_tileSize.x * dx, 0, -_tileSize.z * dz), Quaternion.identity);
            tileInstance.name = "Tile (" + dx + "," + dz + ")";
            _tiles.Add(tileInstance);
        }

    }

}